package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {
        //ToDo: Complete me
    public String attack() {
        return "Menyerang Menggunakan Senjata Api";
    }

    public String getType() {
        return "Gun Type";
    }

}
