package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {
        //ToDo: Complete me
        public String attack() {
            return "Menyerang Menggunakan Pedang";
        }

        public String getType() {
            return "Sword Type";
        }
}
