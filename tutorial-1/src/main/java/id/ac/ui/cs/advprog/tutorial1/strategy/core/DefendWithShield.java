package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {
        //ToDo: Complete me
        public String defend() {
            return "Bertahan Menggunakan Perisai";
        }

        public String getType() {
            return "Shield Type";
        }
}
